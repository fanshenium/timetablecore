module.exports = {
  entry: [
    'webpack-dev-server/client?http://localhost:8080/static/',
    'webpack/hot/only-dev-server',
    './project/assets/js/main.js'
  ],
  output: {
    path: './project/static/',
    filename: 'js/script.js',
    publicPath: 'http://localhost:8080/static/',
    sourceMapFilename: '[file].map'
  },
  module: {
    loaders: [
      {
        test: /\.styl$/,
        loaders: [
          'style-loader',
          'css-loader',
          'autoprefixer-loader?{browsers:["> 5%"]}',
          'stylus-loader'
        ]
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: [
          'react-hot',
          'babel-loader?{presets: ["react", "es2015", "stage-0"], cacheDirectory: ""}'
        ],
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'file-loader?name=images/[name].[hash].[ext]'
      },
      {
        test: /\.(eot|ttf|ijmap|woff|woff2)$/,
        loader: 'file-loader?name=fonts/[name].[hash].[ext]'
      },
    ],
    resolve: {
      extensions: ['', '.js', '.jsx']
    },
  },
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': 'http://localhost:8000',
      'Access-Control-Allow-Credentials': true
    }
  },
  devtool: '#inline-source-map',
  plugins: []
}
