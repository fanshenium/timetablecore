#!/usr/bin/env bash

apt-get update
apt-get upgrade -y
apt-get install -y vim python-software-properties python-dev git git-core python-setuptools libffi-dev libssl-dev libjpeg8-dev libpng12-dev wget default-jdk

add-apt-repository -y ppa:chris-lea/node.js
apt-get update
apt-get install -y nodejs

sudo -u vagrant mkdir -p /home/vagrant/bin
sudo -u vagrant curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > /home/vagrant/bin/lein
chmod a+x /home/vagrant/bin/lein
sudo -u vagrant /home/vagrant/bin/lein

easy_install pip

pip install -U pip setuptools virtualenv requests[security]

if [ ! -f /home/vagrant/.hushlogin ]; then

    virtualenv /env
    chown -R vagrant:vagrant /env
    sudo -u vagrant touch /home/vagrant/.hushlogin

    echo "export PATH=~/bin:node_modules/.bin:\$PATH" >> /home/vagrant/.bashrc
    echo "source /env/bin/activate" >> /home/vagrant/.bashrc
    echo "cd /vagrant" >> /home/vagrant/.bashrc
fi
