import os
import sys


vendor_root = os.path.join(os.path.dirname(__file__), '..', '..', 'vendor')
vendor_root = os.path.normpath(os.path.abspath(vendor_root))
if vendor_root not in sys.path:
    sys.path.insert(0, vendor_root)
