from .common import *  # NOQA

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'timetable'
    }
}

MEDIA_ROOT = path('public', 'media')
STATIC_ROOT = path('public', 'static')

STATIC_URL = 'http://localhost:8080/static/'

INTERNAL_IPS = ('127.0.0.1',)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
