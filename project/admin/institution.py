from django.contrib import admin
from project.models import Institution

from . import admin_site


class InstitutionAdmin(admin.ModelAdmin):

    list_display = ['name', 'slug', 'type']
    search_fields = ['name', 'slug']
    list_filter = ['type']

admin_site.register(Institution, InstitutionAdmin)
