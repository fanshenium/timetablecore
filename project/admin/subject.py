from django.contrib import admin
from project.models import Subject

from . import admin_site


class SubjectAdmin(admin.ModelAdmin):

    list_display = ['name']
    search_fields = ['name']

admin_site.register(Subject, SubjectAdmin)
