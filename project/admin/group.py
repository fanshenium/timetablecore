from django.contrib import admin
from project.models import Group

from . import admin_site


class GroupAdmin(admin.ModelAdmin):

    list_display = ['institution', 'department', 'specialty', 'name', 'grade']
    search_fields = ['name']
    list_select_related = ['department', 'specialty']

    def grade(self, instance):
        return instance.grade
    grade.short_description = 'курс'

admin_site.register(Group, GroupAdmin)
