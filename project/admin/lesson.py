from django.contrib import admin
from django.utils import formats

from project.models import Lesson

from . import admin_site


class LessonAdmin(admin.ModelAdmin):

    list_display = [
        'date', 'group', 'week', 'get_weekday_display', 'time', 'subject', 'teacher',
        'building', 'classroom'
    ]
    list_select_related = ['group', 'subject', 'teacher', 'building']
    search_fields = ['classroom']
    list_filter = ['date', 'building', 'teacher', 'subject', 'classroom']
    date_hierarchy = 'date'

    def week(self, instance):
        return '{} - {}'.format(
            formats.date_format(instance.week.monday(), 'SHORT_DATE_FORMAT'),
            formats.date_format(instance.week.sunday(), 'SHORT_DATE_FORMAT')
        )
    week.short_description = 'неделя'

    def time(self, instance):
        return '{} - {}'.format(
            instance.start_time.strftime('%-H:%M'),
            instance.end_time.strftime('%-H:%M')
        )
    time.short_description = 'время'

admin_site.register(Lesson, LessonAdmin)
