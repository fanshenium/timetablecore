from django.contrib import admin
from project.models import Building

from . import admin_site


class BuildingAdmin(admin.ModelAdmin):

    list_display = ['institution', 'code', 'name', 'address']
    search_fields = ['code', 'name', 'address']

admin_site.register(Building, BuildingAdmin)
