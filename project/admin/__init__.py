from django.contrib.admin import AdminSite as DjangoAdminSite
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.sites.models import Site
from django.contrib.sites.admin import SiteAdmin


class AdminSite(DjangoAdminSite):

    site_title = 'Расписание'
    site_header = 'Расписание'


admin_site = AdminSite()

admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(Site, SiteAdmin)

from . import teacher, department, specialty, subject, group, building, lesson, institution  # NOQA
