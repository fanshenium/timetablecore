from django.contrib import admin
from project.models import Teacher

from . import admin_site


class TeacherAdmin(admin.ModelAdmin):

    list_display = ['institution', 'last_name', 'first_name', 'middle_name', 'academic_rank']
    search_fields = list_display

admin_site.register(Teacher, TeacherAdmin)
