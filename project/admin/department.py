from django.contrib import admin
from project.models import Department

from . import admin_site


class DepartmentAdmin(admin.ModelAdmin):

    list_display = ['institution', 'name', 'short_name']
    search_fields = ['name', 'short_name']

admin_site.register(Department, DepartmentAdmin)
