from django.contrib import admin
from project.models import Specialty

from . import admin_site


class SpecialtyAdmin(admin.ModelAdmin):

    list_display = ['code', 'name']
    search_fields = list_display

admin_site.register(Specialty, SpecialtyAdmin)
