from datetime import timedelta

from isoweek import Week

from .models import Lesson, Group


def get_timetable(group: Group, week: Week):
    lessons = Lesson.objects.select_related('teacher', 'building', 'subject').filter(
        date__range=(week.monday(), week.monday() + timedelta(days=7)),
        group=group
    ).order_by('start_time')
    timetable = [[] for day in range(7)]
    for lesson in lessons:
        timetable[lesson.weekday].append(lesson)

    for day in timetable:
        while day and day[-1] is None:
            day.pop(-1)
    return timetable
