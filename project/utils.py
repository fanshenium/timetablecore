from datetime import datetime, date

from isoweek import Week


def get_week_by_date(d):
    assert isinstance(d, (datetime, date))
    if isinstance(d, datetime):
        d = d.date()
    return Week(*d.isocalendar()[:2])
