from rest_framework import serializers
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from django_geoip.models import Region, City, IpRange


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Region
        fields = ['id', 'name']


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ['id', 'name']


@api_view(['GET'])
def current_location_view(request):
    ip = get_client_ip(request)
    try:
        ip_range = IpRange.objects.by_ip(ip)
    except IpRange.DoesNotExist:
        return Response({})
    if ip_range.country.code != 'RU':
        return Response({})
    data = {}
    if ip_range.region:
        data['region'] = RegionSerializer(ip_range.region).data
    if ip_range.city:
        data['city'] = CitySerializer(ip_range.city).data
    return Response(data)


class SearchMixin:

    def get_queryset(self):
        qs = super().get_queryset()
        q = self.request.GET.get('q', '')
        if q:
            qs = qs.filter(name__contains=q)
        return qs


class RegionListView(SearchMixin, ListAPIView):

    queryset = Region.objects.filter(country__code='RU')
    serializer_class = RegionSerializer


class CityListView(SearchMixin, ListAPIView):

    queryset = City.objects.filter(region__country__code='RU')
    serializer_class = CitySerializer

    def get_queryset(self):
        qs = super().get_queryset()
        region_pk = self.request.GET.get('region_id', None)
        if region_pk is not None:
            try:
                region_pk = int(region_pk)
            except ValueError:
                return City.objects.empty()
            qs = qs.filter(region__pk=region_pk)
        return qs
