from datetime import date

from django.shortcuts import get_object_or_404

from rest_framework import serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response

from project import models
from project.utils import get_week_by_date
from project.timetable import get_timetable


class BuildingSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Building
        fields = ['id', 'code', 'name', 'address']


class SubjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Subject
        fields = ['id', 'name']


class TeacherSerializer(serializers.ModelSerializer):

    name = serializers.CharField(source='short_name')

    class Meta:
        model = models.Teacher
        fields = ['id', 'name']


class LessonSerializer(serializers.ModelSerializer):

    building = BuildingSerializer()
    subject = SubjectSerializer()
    teacher = TeacherSerializer()

    class Meta:
        model = models.Lesson
        exclude = ['group', 'date']


@api_view(['GET'])
def timetable_view(request, group_pk):
    group = get_object_or_404(models.Group, pk=group_pk)
    week = get_week_by_date(date.today())
    timetable = get_timetable(group, week)
    timetable = [
        [LessonSerializer(lesson).data for lesson in day]
        for day in timetable
    ]
    return Response({
        'week': [week.monday(), week.sunday()],
        'lessons': timetable
    })
