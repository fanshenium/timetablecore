from rest_framework import serializers
from rest_framework.generics import ListAPIView

from project import models


class InstitutionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Institution
        fields = ['id', 'name', 'slug', 'type']


class InstitutionListView(ListAPIView):

    queryset = models.Institution.objects.all()
    serializer_class = InstitutionSerializer
    paginate_by = 30
