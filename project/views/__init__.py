from .institution_list import InstitutionListView  # NOQA
from .timetable import timetable_view  # NOQA
from .geoip import current_location_view, RegionListView, CityListView  # NOQA
