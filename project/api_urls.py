from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns(
    '',
    url(r'^current_location/$', views.current_location_view),
    url(r'^region/$', views.RegionListView.as_view()),
    url(r'^city/$', views.CityListView.as_view()),
    url(r'^institutions/$', views.InstitutionListView.as_view()),
    url(r'^timetable/(?P<group_pk>\d+)/$', views.timetable_view),
)
