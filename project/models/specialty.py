from django.db import models


class Specialty(models.Model):

    code = models.CharField('код', max_length=255)
    name = models.CharField('название', max_length=255)

    class Meta:
        verbose_name = 'специальность'
        verbose_name_plural = 'специальности'
        ordering = ('code', 'name')

    def __str__(self):
        return '{} - {}'.format(self.code, self.name)
