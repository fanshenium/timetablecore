from .institution import Institution  # NOQA
from .teacher import Teacher  # NOQA
from .department import Department  # NOQA
from .specialty import Specialty  # NOQA
from .subject import Subject  # NOQA
from .group import Group  # NOQA
from .building import Building  # NOQA
from .lesson import Lesson  # NOQA
