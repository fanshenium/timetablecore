from django.core.validators import ValidationError
from django.db import models

from .institution import Institution


def institution_is_not_school(institution):
    if institution.type == Institution.TYPES.school:
        raise ValidationError('Данное учебное заведение не может иметь факультеты')


class Department(models.Model):

    institution = models.ForeignKey(Institution, verbose_name='учебное заведение',
                                    related_name='departments',
                                    validators=[institution_is_not_school])
    name = models.CharField('название', max_length=255)
    short_name = models.CharField('краткое название', max_length=255)

    class Meta:
        verbose_name = 'факультет'
        verbose_name_plural = 'факультеты'
        ordering = ('name', )

    def __str__(self):
        return self.name
