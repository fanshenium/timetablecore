from django.db import models

from .institution import Institution


class Building(models.Model):

    institution = models.ForeignKey(Institution, verbose_name='учебное заведение',
                                    related_name='institution', null=True)
    code = models.CharField('номер', max_length=255)
    name = models.CharField('название', max_length=255)
    address = models.TextField('адрес')

    class Meta:
        verbose_name = 'корпус'
        verbose_name_plural = 'корпуса'
        ordering = ('institution', 'code')

    def __str__(self):
        return self.name
