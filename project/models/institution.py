from django.db import models

from model_utils import Choices


class Institution(models.Model):

    TYPES = Choices(
        (1, 'school', 'школа'),
        (2, 'college', 'учреждение среднего специального образования'),
        (3, 'university', 'университет'),
    )

    slug = models.SlugField('поддомен', unique=True, blank=True)
    name = models.CharField('название', max_length=255, unique=True)
    type = models.PositiveIntegerField('тип', choices=TYPES)

    class Meta:
        verbose_name = 'учебное заведение'
        verbose_name_plural = 'учебные заведения'
        ordering = ('name', )

    def __str__(self):
        return self.name
