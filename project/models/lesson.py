from django.core.validators import MinValueValidator, MaxValueValidator, ValidationError
from django.db import models

from isoweek import Week

from .group import Group
from .subject import Subject
from .teacher import Teacher
from .building import Building


WEEKDAYS = (
    (0, 'понедельник'),
    (1, 'вторник'),
    (2, 'среда'),
    (3, 'четверг'),
    (4, 'пятница'),
    (5, 'суббота'),
    (6, 'воскресенье'),
)


class LessonManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().extra(
            select={
                'weekday': '(date_part(\'dow\', date)::int - 1) %% 7',
                'week_number': 'date_part(\'week\', date) :: int'
            }
        )


def not_sunday(date):
    if date.weekday() == 6:
        raise ValidationError('Выберите рабочий день недели (не воскресенье)')
    return date


class Lesson(models.Model):

    group = models.ForeignKey(Group, verbose_name='группа', related_name='lessons')
    date = models.DateField('дата')
    subject = models.ForeignKey(Subject, verbose_name='предмет', related_name='lessons',
                                blank=True, null=True)
    teacher = models.ForeignKey(Teacher, verbose_name='преподаватель', related_name='lessons',
                                blank=True, null=True)
    building = models.ForeignKey(Building, verbose_name='корпус', related_name='lessons',
                                 blank=True, null=True)
    classroom = models.CharField('кабинет', max_length=255, blank=True)
    start_time = models.TimeField('время начала')
    end_time = models.TimeField('время окончания')

    objects = LessonManager()

    class Meta:
        verbose_name = 'занятие'
        verbose_name_plural = 'занятия'
        ordering = ('date', 'group', 'start_time')

    def __str__(self):
        return '{} {} {}'.format(self.group, self.date, self.get_time_display())

    def get_weekday_display(self):
        return dict(WEEKDAYS)[self.weekday]
    get_weekday_display.short_description = 'день недели'

    @property
    def week(self):
        return Week(self.date.year, self.week_number)

    def get_time_display(self):
        return '{} - {}'.format(
            self.start_time.strftime('%-H:%M'),
            self.end_time.strftime('%-H:%M')
        )
