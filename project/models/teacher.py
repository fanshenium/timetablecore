from django.db import models

from model_utils import Choices

from .institution import Institution


class Teacher(models.Model):

    # FIXME: Надо добавить другие звания
    ACADEMIC_RANKS = Choices(
        (0, 'senior_teacher', 'ст. пр.'),
        (1, 'docent', 'доц.'),
        (2, 'professor', 'проф.'),
    )

    institution = models.ForeignKey(Institution, verbose_name='учебное заведение',
                                    related_name='teachers', null=True)

    last_name = models.CharField('фамилия', max_length=255)
    first_name = models.CharField('имя', max_length=255)
    middle_name = models.CharField('отчество', max_length=255)

    academic_rank = models.PositiveIntegerField('учёное звание', choices=ACADEMIC_RANKS,
                                                blank=True)

    class Meta:
        verbose_name = 'преподаватель'
        verbose_name_plural = 'преподаватели'
        ordering = ('institution', 'last_name', 'first_name', 'middle_name')

    def __str__(self):
        return self.short_name

    @property
    def short_name(self):
        return '{} {} {}. {}.'.format(
            self.get_academic_rank_display(),
            self.last_name,
            self.first_name[0],
            self.middle_name[0],
        )

    @property
    def full_name(self):
        return '{} {} {}'.format(
            self.last_name,
            self.first_name,
            self.middle_name,
        )
