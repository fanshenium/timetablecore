from datetime import date

from django.core.validators import ValidationError
from django.db import models

from .specialty import Specialty
from .department import Department
from .institution import Institution


class GroupManager(models.Manager):

    def get_queryset(self):
        today = date.today()
        return super().get_queryset().extra(
            select={'grade': '%s - income_year' + (' + 1' if 9 <= today.month <= 12 else '')},
            select_params=(today.year, )
        )


class Group(models.Model):

    institution = models.ForeignKey(Institution, verbose_name='учебное заведение',
                                    related_name='groups', null=True)
    department = models.ForeignKey(Department, verbose_name='факультет', related_name='groups',
                                   null=True, blank=True)
    specialty = models.ForeignKey(Specialty, verbose_name='специальность', related_name='groups',
                                  null=True, blank=True)
    income_year = models.PositiveIntegerField('год поступления')
    graduation_year = models.PositiveIntegerField('год окончания')
    name = models.CharField('название', max_length=255)

    objects = GroupManager()

    class Meta:
        verbose_name = 'группа'
        verbose_name_plural = 'группы'
        ordering = ('department', 'specialty', 'name')

    def __str__(self):
        return self.name

    def clean(self):
        if self.department and self.department.institution != self.institution:
            raise ValidationError('Учебное заведение не совпадает с учебным заведением факультета')
        return super().clean()

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)
