import React from 'react';
import { createHistory } from 'history';
import { Router, Route } from 'react-router';
import { syncReduxAndRouter } from 'redux-simple-router'

import store from './store';
import RootView from './components/RootView';

const history = createHistory()

syncReduxAndRouter(history, store);

export default () => (
  <Router history={ history }>
    <Route path="/" component={ RootView } />
    <Route path="*" component={ () => <div>Not Found</div> } />
  </Router>
);
