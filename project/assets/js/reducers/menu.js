export default (state = false, {type}) => {
  switch (type) {
    case 'MENU/OPEN':
      return true;
    case 'MENU/CLOSE':
      return false;
    default:
      return state;
  }
}
