import { combineReducers } from 'redux';
import { routeReducer } from 'redux-simple-router'

import menu from './menu';

export default combineReducers({
  displayMenu: menu,
  routing: routeReducer
});
