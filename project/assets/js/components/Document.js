import React from 'react';


export default class Document extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'Document';
  }

  componentDidMount() {
    document.title = this.props.title;
  }

  componentWillReceiveProps() {
    document.title = this.props.title;
  }

  render() {
    return <div {...this.props} />;
  }
}
