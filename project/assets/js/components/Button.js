import React from 'react';
import Link from './link';

export default class Button extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'Button';
  }

  render() {
    if (this.props.href) {
      return (
        <Link
          href={ this.props.href }
          className={ 'button ' + this.props.className }
          onClick={ this.props.onClick }
        >
          { this.props.children }
        </Link>
      );
    } else {
      return (
        <button
          id={ this.props.id }
          className={ 'button ' + this.props.className }
          onClick={ this.props.onClick }
        >
          { this.props.children }
        </button>
      );
    }
  }
}
