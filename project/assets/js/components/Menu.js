import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import classSet from 'react-classset';
import { connect } from 'react-redux';

import Link from './link';
import { menu } from '../actions'


class Menu extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'Menu';
  }

  render() {
    const { props } = this;
    const logoUrl = require('../../images/logo.png');
    return (
      <div>
        <div
          id='header'
          className='nav-bar'
        >
          <div className='content'>
            <span className='icons menu-icon' onClick={ props.menuOpen }>menu</span>
            <Link href='/' className='logo-a'>
              <img
                src={ logoUrl }
                alt={ logoUrl }
                className='logo'
                width='50px'
              />
            </Link>
            <div className='menu-elements-top'>
              { props.children }
            </div>
          </div>
        </div>
        <MenuWrapper show={ props.showWrapper } onClick={ props.menuClose }>
          { props.children }
        </MenuWrapper>
      </div>
    );
  }
}


class MenuWrapper extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'MenuWrapper'
  }

  render() {
    const { props } = this;
    return (
      <ReactCSSTransitionGroup
        transitionName='menu-transition'
        transitionEnterTimeout={ 300 }
        transitionLeaveTimeout={ 300 }
      >
        { props.show ?
            (
              <div className='menu-content'>
                <div className='menu-wrapper' onClick={ props.onClick } />
                <div className='menu-elements-left'>
                  <div className='menu-logo'>Sh</div>
                  { props.children }
                </div>
              </div>
            ) :
            null
          }
      </ReactCSSTransitionGroup>
    );
  }
}

export default connect(
  (state) => ({
    showWrapper: state.displayMenu
  }),
  (dispatch) => ({
    menuOpen: () => {
      dispatch(menu.open());
    },
    menuClose: () => {
      dispatch(menu.close());
    }
  })
)(Menu)
