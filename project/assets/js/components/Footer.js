import React from 'react';


export default class Footer extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'Footer';
  }

  render() {
    return (
      <div id={ this.props.id }>
        <div id='footer-wrapper' className='content'>
          <div id='footer-left-side'>
            <div className='footer-block' key={ 1 }>
              <span className='footer-title'>Язык</span>
              <span>Русский</span>
            </div>
            <div className='footer-block' key={ 2 }>
              <span className='footer-title'>Помошь</span>
              <span>Как найти свое расписание</span>
              <span>Создание расписания мечты</span>
            </div>
          </div>
          <div id='footer-right-side'>
            <div className='footer-block' key={ 1 }>
              <span className='footer-title'>Контакты</span>
              <a href='mailto:example@example.example' target='_blank'>example@example.example</a>
              <span>Оставить отзыв</span>
            </div>
            <div className='footer-block' key={ 2 }>
              <span className='footer-title'>О нас</span>
              <a href='https://vk.com/fletcher35' target='_blank'>Тимофей Кукушкин</a>
              <a href='https://vk.com/donatgorbachev' target='_blank'>Донат Горбачев</a>
              <span>
                { '2015' + (new Date().getFullYear() != 2015 ? `\u2012#{new Date()).getFullYear()}` : '') }
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
