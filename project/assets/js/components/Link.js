import React from 'react';
import { connect } from 'react-redux';
import { pushPath } from 'redux-simple-router';

class Link extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'Link';
  }

  clickHandler(event) {
    event.preventDefault();
    const { props } = this;
    props.dispatch(pushPath(props.href));
  }

  render() {
    return (
      <a { ...this.props } onClick={ this.clickHandler.bind(this) }/>
    );
  }
}

export default connect()(Link);
