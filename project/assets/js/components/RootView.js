import React from 'react';

import Document from './Document';
import Menu from './Menu';
import Footer from './Footer';
import Button from './Button';


export default class RootView extends React.Component {

  constructor(props) {
    super(props);
    this.displayName = 'Root';
  }

  render() {
    const { props } = this;
    return (
      <Document title={ this.props.title }>
        <Menu>
          <Button className='menu-button' href='/path1' key={ 1 }>Последние 5</Button>
          <Button className='menu-button' href='/path2' key={ 2 }>Войти</Button>
        </Menu>
        <div id='content'>
          { props.children }
        </div>
        <Footer id='footer' />
      </Document>
    );
  }
}
