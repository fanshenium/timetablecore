import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import DevTools from './devtools';
import reducer from './reducers';

const store = compose(
  applyMiddleware(thunk),
  DevTools.instrument()
)(createStore)(reducer);

if (module.hot) {
  module.hot.accept('./reducers', () =>
    store.replaceReducer(require('./reducers'))
  );
}

export default store;
