import '../css/style.styl';

import React from 'react'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './store';
import Router from './router';
import DevTools from './devtools';

ReactDOM.render(
  <Provider store={ store }>
    <div>
      <Router />
      <DevTools />
    </div>
  </Provider>,
  document.getElementById('root')
);
