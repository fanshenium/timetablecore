const open = () => ({
  type: 'MENU/OPEN'
});

const close = () => ({
  type: 'MENU/CLOSE'
});

export { open, close }
