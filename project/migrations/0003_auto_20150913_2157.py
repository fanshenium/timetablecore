# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_auto_20150913_2150'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='building',
            options={'verbose_name_plural': 'корпуса', 'verbose_name': 'корпус', 'ordering': ('institution', 'code')},
        ),
        migrations.AlterModelOptions(
            name='teacher',
            options={'verbose_name_plural': 'преподаватели', 'verbose_name': 'преподаватель', 'ordering': ('institution', 'last_name', 'first_name', 'middle_name')},
        ),
        migrations.AddField(
            model_name='building',
            name='institution',
            field=models.ForeignKey(related_name='institution', verbose_name='учебное заведение', to='project.Institution', null=True),
        ),
    ]
