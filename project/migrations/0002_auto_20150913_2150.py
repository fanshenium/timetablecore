# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='institution',
            field=models.ForeignKey(to='project.Institution', verbose_name='учебное заведение', null=True, related_name='teachers'),
        ),
        migrations.AlterField(
            model_name='department',
            name='institution',
            field=models.ForeignKey(to='project.Institution', verbose_name='учебное заведение', related_name='departments'),
        ),
    ]
