# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0003_auto_20150913_2157'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='lesson',
            options={'verbose_name_plural': 'занятия', 'ordering': ('date', 'group', 'start_time'), 'verbose_name': 'занятие'},
        ),
        migrations.AddField(
            model_name='group',
            name='institution',
            field=models.ForeignKey(related_name='groups', null=True, verbose_name='учебное название', to='project.Institution'),
        ),
        migrations.AlterField(
            model_name='group',
            name='department',
            field=models.ForeignKey(related_name='groups', null=True, verbose_name='факультет', to='project.Department', blank=True),
        ),
    ]
