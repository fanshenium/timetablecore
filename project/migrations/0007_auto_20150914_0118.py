# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0006_institution_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='slug',
            field=models.SlugField(unique=True, blank=True, null=True, verbose_name='поддомен'),
        ),
    ]
