# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0007_auto_20150914_0118'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='поддомен', blank=True),
        ),
    ]
