# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('code', models.CharField(verbose_name='номер', max_length=255)),
                ('name', models.CharField(verbose_name='название', max_length=255)),
                ('address', models.TextField(verbose_name='адрес')),
            ],
            options={
                'verbose_name': 'корпус',
                'verbose_name_plural': 'корпуса',
                'ordering': ('code',),
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='название', max_length=255)),
                ('short_name', models.CharField(verbose_name='краткое название', max_length=255)),
            ],
            options={
                'verbose_name': 'факультет',
                'verbose_name_plural': 'факультеты',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('income_year', models.PositiveIntegerField(verbose_name='год поступления')),
                ('graduation_year', models.PositiveIntegerField(verbose_name='год окончания')),
                ('name', models.CharField(verbose_name='название', max_length=255)),
                ('department', models.ForeignKey(related_name='groups', to='project.Department', verbose_name='факультет')),
            ],
            options={
                'verbose_name': 'группа',
                'verbose_name_plural': 'группы',
                'ordering': ('department', 'specialty', 'name'),
            },
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='название', max_length=255, unique=True)),
            ],
            options={
                'verbose_name': 'учебное заведение',
                'verbose_name_plural': 'учебные заведения',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date', models.DateField(verbose_name='дата')),
                ('classroom', models.CharField(verbose_name='кабинет', max_length=255, blank=True)),
                ('start_time', models.TimeField(verbose_name='время начала')),
                ('end_time', models.TimeField(verbose_name='время окончания')),
                ('building', models.ForeignKey(related_name='lessons', blank=True, to='project.Building', verbose_name='корпус', null=True)),
                ('group', models.ForeignKey(related_name='lessons', to='project.Group', verbose_name='группа')),
            ],
            options={
                'verbose_name': 'занятие',
                'verbose_name_plural': 'занятия',
                'ordering': ('date',),
            },
        ),
        migrations.CreateModel(
            name='Specialty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('code', models.CharField(verbose_name='код', max_length=255)),
                ('name', models.CharField(verbose_name='название', max_length=255)),
            ],
            options={
                'verbose_name': 'специальность',
                'verbose_name_plural': 'специальности',
                'ordering': ('code', 'name'),
            },
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='название', max_length=255)),
            ],
            options={
                'verbose_name': 'предмет',
                'verbose_name_plural': 'предметы',
            },
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('last_name', models.CharField(verbose_name='фамилия', max_length=255)),
                ('first_name', models.CharField(verbose_name='имя', max_length=255)),
                ('middle_name', models.CharField(verbose_name='отчество', max_length=255)),
                ('academic_rank', models.PositiveIntegerField(verbose_name='учёное звание', blank=True, choices=[(0, 'ст. пр.'), (1, 'доц.'), (2, 'проф.')])),
            ],
            options={
                'verbose_name': 'преподаватель',
                'verbose_name_plural': 'преподаватели',
                'ordering': ('last_name', 'first_name', 'middle_name'),
            },
        ),
        migrations.AddField(
            model_name='lesson',
            name='subject',
            field=models.ForeignKey(related_name='lessons', blank=True, to='project.Subject', verbose_name='предмет', null=True),
        ),
        migrations.AddField(
            model_name='lesson',
            name='teacher',
            field=models.ForeignKey(related_name='lessons', blank=True, to='project.Teacher', verbose_name='преподаватель', null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='specialty',
            field=models.ForeignKey(related_name='groups', blank=True, to='project.Specialty', verbose_name='специальность', null=True),
        ),
        migrations.AddField(
            model_name='department',
            name='institution',
            field=models.ForeignKey(verbose_name='учебное заведение', to='project.Institution'),
        ),
    ]
