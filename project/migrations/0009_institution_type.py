# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0008_auto_20150914_0119'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='type',
            field=models.PositiveIntegerField(default=3, choices=[(1, 'школа'), (2, 'учреждение среднего специального образования'), (3, 'университет')], verbose_name='тип'),
            preserve_default=False,
        ),
    ]
