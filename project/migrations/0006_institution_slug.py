# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0005_auto_20150913_2224'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='slug',
            field=models.SlugField(null=True, blank=True, verbose_name='поддомен', unique=True, default=None),
        ),
    ]
