# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0004_auto_20150913_2210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='institution',
            field=models.ForeignKey(related_name='groups', verbose_name='учебное заведение', to='project.Institution', null=True),
        ),
    ]
