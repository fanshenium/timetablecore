from django.apps import AppConfig


class ProjectConfig(AppConfig):
    name = 'project'
    label = 'project'
    verbose_name = 'Расписание'
