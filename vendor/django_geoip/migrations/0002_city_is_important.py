# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_geoip', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='is_important',
            field=models.BooleanField(default=False, verbose_name='важный'),
        ),
    ]
