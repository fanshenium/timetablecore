(defproject cljs-build "1.0.0"
  :dependencies [[org.clojure/clojure "1.7.0-beta2"]
                 [org.clojure/clojurescript "0.0-3308" :exclusions [org.apache.ant/ant]]]
  :plugins [[lein-cljsbuild "1.0.6"]]
  :cljsbuild {
    :builds [{
      :source-paths ["project/assets/cljs"]
      :compiler {
        :output-dir "project/static/js"
        :output-to "project/static/js/main.js"
        :omptimizations :whitespace
        :pretty-print true
        :warnings {
          :single-segment-namespace false}}}]})
