import os
from project.settings.common import *  # NOQA

TEST_APP_DIR = os.path.dirname(__file__)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(TEST_APP_DIR, 'test.db')
    }
}

INSTALLED_APPS += ('tests', )
